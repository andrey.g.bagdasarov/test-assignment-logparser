package com.logparser.service;

import java.time.LocalDate;
import java.util.Date;

public class LogParts {

    private LocalDate date;
    private Date timestamp;
    private String threadId; //(in brackets)
    private String userContext;  //optional (in square brackets)
    private String requestedResource; //(one string)
    private String dataPayload; // (0..n elements)
    private Double requestDuration; //in milliseconds


    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getThreadId() {
        return threadId;
    }

    public void setThreadId(String threadId) {
        this.threadId = threadId;
    }

    public String getUserContext() {
        return userContext;
    }

    public void setUserContext(String userContext) {
        this.userContext = userContext;
    }

    public String getRequestedResource() {
        return requestedResource;
    }

    public void setRequestedResource(String requestedResource) {
        this.requestedResource = requestedResource;
    }

    public String getDataPayload() {
        return dataPayload;
    }

    public void setDataPayload(String dataPayload) {
        this.dataPayload = dataPayload;
    }

    public Double getRequestDuration() {
        return requestDuration;
    }

    public void setRequestDuration(Double requestDuration) {
        this.requestDuration = requestDuration;
    }
}
