package com.logparser.service;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class LogParser {

    public void parseLogFile(String path, Integer topRequests, String timeFormat) {
        //Map to keep resource and avg duration
        Map<String, Double> resDur = new HashMap<>();
        //Map to keep resource count
        Map<String, Integer> resCount = new HashMap<>();
        //Map for request timing
        //Using treeMap to keep Sorted elements in map
        TreeMap<Date, Integer> reqTime = new TreeMap<>();
        try {
            Stream<String> lines = Files.lines(Paths.get(path));
            lines.forEach(s -> {
                LogParts lp = parseLogStringToObject(s);
                collectRequestDurations(lp, resDur, resCount);
                calculateHourlyRequests(lp, reqTime, timeFormat);
            });
            //Calculate average duration;
            resDur.forEach((k, v) -> {
                int c = resCount.get(k);
                resDur.put(k, v / c);
            });
            printTopRequests(resDur, topRequests);
            drawHistogram(reqTime);
        } catch (Exception e) {
            //System.out.println(e.getCause());
        }
    }

    /*From log string to POJO*/
    private LogParts parseLogStringToObject(String str) {
        Pattern p = Pattern.compile("^(?<dt>.*?)\\s(?<tm>.*?)\\s(?<tr>.*?)\\s(?<ct>.*?)\\s(?<rs>.*?)\\s.*?in\\s(?<dr>.*?)$");
        Matcher m = p.matcher(str);
        LogParts lp = new LogParts();
        while (m.find()) {
            lp.setDate(stringToDate(m.group("dt"), "yyyy-MM-dd"));
            lp.setTimestamp(stringToTime(m.group("tm"), "H:mm:ss"));
            lp.setThreadId(m.group("tr"));
            lp.setUserContext(m.group("ct"));
            lp.setRequestedResource(parseURLString(m.group("rs")));
            lp.setRequestDuration(Double.parseDouble(m.group("dr")));
        }
        return lp;
    }


    private String parseURLString(String val){
        Pattern p;
        Matcher m;
        if(val.contains("action")){
           String[] s = val.split("\\&");
           return s[0];
        }
        if(!val.contains("?") && !val.contains(".")){
            String[] s = val.split("\\s");
            return s[0];
        }
        if(!val.contains("?") && val.contains(".")){
            return val;
        }
        else {
            String[] s = val.split("\\?");
            return s[0];
        }
    }

    private void collectRequestDurations(LogParts lp, Map<String, Double> resDur, Map<String, Integer> resCount) {
        String res = lp.getRequestedResource();
        Double dur = lp.getRequestDuration();
        //Duration
        if (!resDur.containsKey(res)) { //if no duration added yet
            resDur.put(res, dur);
            resCount.put(res, 1);
        } else {
            int c = resCount.get(res); //
            resCount.put(res, ++c);   //increment for every duration
            Double d = resDur.get(res); //total duration amount
            resDur.put(res, (d + dur)); //Calculate new total duration amount
        }
    }

    private Map<String, Double> sortMapByValue(Map<String, Double> unsortedMap) {
        List<Map.Entry<String, Double>> list = new ArrayList<>(unsortedMap.entrySet());

        list.sort(new Comparator<Map.Entry<String, Double>>() {
            @Override
            public int compare(Map.Entry<String, Double> o1, Map.Entry<String, Double> o2) {
                return o2.getValue().compareTo(o1.getValue());
            }
        });
        Map<String, Double> sortedMap = new LinkedHashMap<>();
        for (Map.Entry<String, Double> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        return sortedMap;
    }


    private void calculateHourlyRequests(LogParts lp, TreeMap<Date, Integer> reqTime, String format) {
        Date time = lp.getTimestamp();
        if (format.equals("h")) {
            time = roundToHour(roundToMinute(time));
        } else {
            time = roundToMinute(time);
        }
        if (!reqTime.containsKey(time)) {
            reqTime.put(time, 1);
        } else {
            int reqCount = reqTime.get(time);
            reqTime.put(time, reqCount + 1);
        }
    }

    private Date roundToMinute(Date time) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(time);
        if (cal.get(Calendar.SECOND) >= 60) {
            cal.add(Calendar.MINUTE, 1);
        } else {
            cal.set(Calendar.SECOND, 0);
        }
        return cal.getTime();
    }

    private Date roundToHour(Date time) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(time);
        if (cal.get(Calendar.MINUTE) >= 60) {
            cal.add(Calendar.HOUR, 1);
        } else {
            cal.set(Calendar.MINUTE, 0);
        }
        return cal.getTime();
    }


    private void drawHistogram(TreeMap<Date, Integer> reqTimes) {
        System.out.println("Time -> request count histogram ");
        System.out.println();
        reqTimes.forEach((k, v) -> {
            SimpleDateFormat localDateFormat = new SimpleDateFormat("HH:mm:ss");
            String time = localDateFormat.format(k);
            //Trying to deal with line length
            int count = v;
            if (v > 50) {
                v = v / 2;
            }
            if (v > 200) {
                v = v / 4;
            }
            if (v > 400) {
                v = v / 8;
            }
            System.out.print(time);
            System.out.print(" ");
            for (int i = 0; i <= v - 1; i++) {
                System.out.print("*");
            }
            System.out.print(" " + count + " requests");
            System.out.println();
        });
    }

    private void printTopRequests(Map<String, Double> reqMap, Integer count) {
        reqMap = sortMapByValue(reqMap);
        ArrayList<String> urls = new ArrayList<>(reqMap.keySet());
        ArrayList<Double> durs = new ArrayList<>(reqMap.values());
        DecimalFormat df = new DecimalFormat("#.##");
        System.out.println();
        System.out.println("TOP " + count + " of highest average duration");
        System.out.println();

        //If user wants to see more requests then we have
        // we will show all requests
        if (urls.size() < count) {
            count = urls.size();
        }
        for (int i = 0; i < count; i++) {
            System.out.print(urls.get(i));
            System.out.print("  ");
            System.out.println(df.format(durs.get(i)));
        }
        System.out.println();
    }


    private static LocalDate stringToDate(String str, String pattern) {
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern(pattern);
        LocalDate date = LocalDate.parse(str, fmt);
        return date;
    }

    private static Date stringToTime(String time, String pattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        Date tm = null;
        try {
            tm = new Date(sdf.parse(time).getTime());
        } catch (Exception ex) {
            System.out.println("Could not parse string " + time + " to time");
        }
        return tm;
    }
}
