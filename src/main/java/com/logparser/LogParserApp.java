package com.logparser;

import com.logparser.service.LogParser;

import java.util.*;


public class LogParserApp {


    public static void main(String[] args) {
        Long workTime = System.currentTimeMillis();
        String path = "";
        //By default show top 3 avg request timing
        Integer topReqCount = 3;
        String timeFormat = "";
        /*Handling commandline args*/
        if (args.length > 0 && args[0].equals("-h")) {
            printHelp();
            System.exit(0);
        } else if (args.length > 0) {
            path = args[0];
            if (args.length > 1) {
                try {
                    topReqCount = Integer.parseInt(args[1]);
                } catch (Exception e) {
                    System.out.println("Incorrect second parameter! Should be a number.");
                    System.exit(0);
                }

            }
            if (args.length > 2 && args[2].equals("h")) {
                timeFormat = args[2];
            }

        } else {
            System.out.println("No file path was provided");
        }
        /*Magic starts here*/
        LogParser lp = new LogParser();
        lp.parseLogFile(path, topReqCount, timeFormat);

        Long millisec = (System.currentTimeMillis() - workTime);
        Long sec = (millisec / 1000);
        System.out.println();
        System.out.println("Program run for (" + millisec + ")" + sec + " (milli)seconds");
    }

    //TODO Move to file
    private static void printHelp() {
        System.out.println("Help information\n" +
                "\n" +
                " First parameter  - path to the log file  \n" +
                "\t\t\t\t\tExample C:\\logfile.log\n" +
                " Second parameter - count of top requests with highest duration time, by default will display top 3\n" +
                "\t\t\t\t\tExample C:\\logfile.log 5\n" +
                " Third parameter  - time format to display of hourly(h)/minute(m) number of requests.\n" +
                "                    Example C:\\logfile.log 5 m\n" +
                " -h    Displays help information.");
    }
}
